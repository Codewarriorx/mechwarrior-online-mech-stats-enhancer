// ==UserScript==
// @name       Mechwarrior Mech Stats Enhancer
// @namespace  http://use.i.E.your.homepage/
// @version    2.3.1
// @description  enter something useful
// @match      http://mwomercs.com/profile/stats?type=mech
// @copyright  2013+, Michael Lauridsen
// ==/UserScript==
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js

statEnhancer = {
	sessions: {},
	records: [],
	matchTracker: false,
	overallTotals: [],

	initialize: function(){
		this.modUI();
		this.addEvents();
		var sessionAvailable = this.loadSessions();
		this.parseTable();
		this.calcTotals();
		this.outputTotals();

		$('.dataTable tr').find('td, th').css('text-align','center').end().find('td:eq(0), th:eq(0)').css('text-align','right');

		if(sessionAvailable){ // auto compare against latest session
			this.compareSession($('#sessions').val());
		}
	},

	modUI: function(){
		$('.table').addClass('dataTable');
		var thead = $('thead');

		// add column headers
		thead.find('tr > th').eq(9).before('<th>DPM</th>');
		thead.find('tr > th').eq(11).before('<th>XPM</th>');

		// add button
		$('h2').after('<button id="deleteSession">Delete Session</button>')
			.after('<button id="saveSession">Start Session</button>')
			.after('<select id="sessions"></select>');
	},

	addEvents: function(){
		var scope = this;
		$('body').on('click', '#deleteSession', function(){
			scope.deleteSession(scope.sessions);
		});
		$('body').on('click', '#saveSession', function(){
			scope.saveSession(scope.sessions);
		});
		$('body').on('change', '#sessions', function(){
			scope.changeSession(scope.sessions, this);
		});
	},

	parseTable: function(){
		var rows = $('.dataTable tbody').find('tr:not(.totals)');
		var records = [];

		rows.each(function(index, row){
			var cells = $(row).find('td');

			var dpm = Math.ceil(parseInt(cells.eq(8).text().replace(/\,/g,''),10)/parseInt(cells.eq(1).text().replace(/\,/g,''),10));
			var xpm = Math.ceil(parseInt(cells.eq(9).text().replace(/\,/g,''),10)/parseInt(cells.eq(1).text().replace(/\,/g,''),10));
			
			cells.eq(9).before('<td>'+dpm+'</td>');
			cells.eq(10).before('<td>'+xpm+'</td>');

			var mech = [];
			cells = $(row).find('td');

			cells.each(function(index, cell){
				if(index != 0 && index != 12){
					mech.push(parseFloat($(cell).text().replace(/\,/g,''),10));
				}
				else{
					mech.push($(cell).text());
				}
			});

			records.push(mech);
		});

		this.records = records;
	},

	calcTotals: function(){
		var scope = this;
		var overallTotals = [];

		this.records.forEach(function(mechRecords, mechIndex, arr){
			mechRecords.forEach(function(record, index, arr){
				if(mechIndex != 0){
					if(index == 0){
						overallTotals[index] += 1;
					}
					else if(index == 12){
						var timeObj = scope.parseTime(record);

						overallTotals[index].hours += timeObj.hours;
						overallTotals[index].minutes += timeObj.minutes;
						overallTotals[index].seconds += timeObj.seconds;
					}
					else{
						overallTotals[index] += record;//parseInt(record.replace(/\,/g,''),10);
					}
				}
				else{
					if(index == 0){
						overallTotals[index] = 1;
					}
					else if(index == 12){
						overallTotals[index] = scope.parseTime(record);
					}
					else{
						overallTotals[index] = record;//parseInt(record.replace(/\,/g,''),10);
					}
				}
			});
		});

		// calc win ratio
		overallTotals[4] = (overallTotals[2] / overallTotals[3]).toFixed(2);

		// calc kill ratio
		overallTotals[7] = (overallTotals[5] / overallTotals[6]).toFixed(2);

		// calc overall DPM
		overallTotals[9] = Math.ceil(overallTotals[8] / overallTotals[1]);

		// calc overall XPM
		overallTotals[11] = Math.ceil(overallTotals[10] / overallTotals[1]);

		// convert time object to string and deal with minutes and seconds
		overallTotals[12].hours = overallTotals[12].hours + parseInt((overallTotals[12].minutes / 60).toFixed(0)) + parseInt(((overallTotals[12].seconds / 60) / 60).toFixed(0));
		overallTotals[12].minutes = overallTotals[12].minutes % 60;
		overallTotals[12].seconds = overallTotals[12].seconds % 60;
		overallTotals[12] = overallTotals[12].hours+':'+('0'+overallTotals[12].minutes).slice(-2)+':'+('0'+overallTotals[12].seconds).slice(-2);

		this.overallTotals = overallTotals;
	},

	parseTime:function(time){
		var explodedTime = time.split(':');
		
		return {
			hours: parseInt(explodedTime[0]),
			minutes: parseInt(explodedTime[1]),
			seconds: parseInt(explodedTime[2])
		};
	},

	outputTotals: function(){
		$('.dataTable tbody').append('<tr class="totals"></tr>');
		this.overallTotals.forEach(function(ele, index, arr){
			$('.totals').append('<td>'+ele+'</td>');
		});

		$('.totals').find('td').css('border-top', '6px solid'); //css('border-top', '6px solid').
	},

	/*	#####################	Session Functions	#########################	*/
	saveSession: function(sessions){
		console.log('Session Started.');
		var date = new Date();

		sessions[date.getTime().toString()] = {"timestamp": date.getTime(), "date": date.toString(), "data": this.records};

		localStorage["mechwarrior"] = JSON.stringify(sessions);
	},

	deleteSession: function(sessions){
		if($('#sessions').val() != ''){
			delete sessions[$('#sessions').val()];
			$('option[value="'+$('#sessions').val()+'"]').remove();
			console.log('session deleted');

			if($('#sessions option').length != 0){
				$('#sessions option').last().attr('selected', 'selected');
				this.compareSession($('#sessions').val());
			}
		}

		localStorage["mechwarrior"] = JSON.stringify(this.sessions);
	},

	changeSession: function(sessions, ele){
		$('.dataTable td').find('br, .change').remove();
		this.compareSession($(ele).val());
	},

	loadSessions: function(){
		if(typeof localStorage["mechwarrior"] !== "undefined"){
			this.sessions = JSON.parse(localStorage["mechwarrior"]);

			for(var obj in this.sessions){
				$('#sessions').append('<option value="'+this.sessions[obj].timestamp+'">'+this.sessions[obj].date+'</option>');
			}

			$('#sessions option').last().attr('selected', 'selected');

			return true;
		}
		return false;
	},

	compareSession: function(sessionID){
		var thead = $('.dataTable thead');
		var rows = $('.dataTable tbody').find('tr:not(.totals)');

		var session = this.sessions[sessionID];
		
		rows.each(function(index, row){
			var cells = $(row).find('td');
			
			var mechExists = false;
			var mechIndex = index;
			do{
				if(session.data[mechIndex][0] == $(cells[0]).text()){
					mechExists = true;
				}
				else{
					mechIndex++;
				}
			}while(!mechExists && mechIndex < session.data.length);

			if(mechExists){
				cells.each(function(index, cell){
					if(index != 0 && index != 12){
						var data = parseFloat($(cell).text().replace(/\,/g,''),10);
					}
					else{
						var data = $(cell).text();
					}

					if(index != 0 && index != 12){
						var diff = (data - session.data[mechIndex][index]).toFixed(2);

						if(session.data[mechIndex][index] < data){
							$(cell).append('<br><span class="change">('+diff+'&Delta;)</span>');
							$(cell).css('color','#00ae57');
						}
						else if(session.data[mechIndex][index] == data){
							$(cell).css('color','#ffff00');
						}
						else{
							$(cell).append('<br><span class="change">('+diff+'&Delta;)</span>');
							$(cell).css('color','#ff0000');
						}

					}
					
				});
			}

		});
	}

};

statEnhancer.initialize();